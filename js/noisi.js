var xoff1 = 0
var xoff2 = 10000
var inc = 0.01
var start = 0
var capture

function setup(){
	createCanvas(window.innerWidth, window.innerHeight)
	capture = createCapture(VIDEO)
	capture.size(240, 240)
	capture.elt.setAttribute('playsinline', '')
	capture.hide()
}

function draw(){
	var x = map(noise(xoff1), 0, 1, 0, width)
	var y = map(noise(xoff2), 0, 1, 0, height)

	xoff1 += 0.02
	xoff2 += 0.02
	noStroke()
	tint(255, 128)
	// rect(x, y, 24, 24)
	image(capture, x, y, 24, 24)
	if (mouseIsPressed){
		xoff1 = mouseX
		// x = map(xoff1, 0, 1, 0, width)
		xoff2 = mouseY
		// y = map(xoff2, 0, 1, 0, height)
	}
}
