
var capture;
var canvas


function setup() {
  canvas = createCanvas(window.innerWidth, window.innerHeight);
  background(51);
  capture = createCapture(VIDEO);
  capture.size(window.innerWidth, window.innerHeight);
  capture.elt.setAttribute('playsinline', '');
  capture.hide();
  canvas.position(0, 0)

  canvas.style('z-index', '-1')
  canvas.style('opacity', '0.9')

}



function draw() {
  // tint(255, 0, 0);
  push()
  translate(capture.width, 0)
  scale(-1.0, 1.0);  
  

  image(capture, mouseY, mouseX, mouseY, mouseX);
  image(capture, mouseX, mouseY, mouseX, mouseY);
  image(capture, mouseX, mouseX, mouseY, mouseY);
  image(capture, mouseY, mouseY, mouseX, mouseX);
  pop()
}

function touchMoved(){
  push()
  translate(capture.width, 0)
  scale(-1.0, 1.0);  
  image(capture, mouseY, mouseX, mouseY, mouseX);
  image(capture, mouseX, mouseY, mouseX, mouseY);
  image(capture, mouseX, mouseX, mouseY, mouseY);
  image(capture, mouseY, mouseY, mouseX, mouseX);
  pop()   
}

 function keyPressed(){
  if(keyCode === 32){
    saveCanvas(canvas, 'intercam', '.png')
  } console.log(keyCode)
 }
