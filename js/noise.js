let noiseScale = 2 


 function setup(){
	createCanvas(window.innerWidth, window.innerHeight)

 }


function draw() {
  
  for (let x = 0; x < width; x++) {
    let noiseVal = noise((mouseX+x) * noiseScale, mouseY * noiseScale);
    stroke(noiseVal*255);
    line(mouseX, mouseY+noiseVal * 50, x, height);
  }
}